var test = require('tape');
var analyser = require('./../server/analyser.js');

// test('Search Dictionary', function(t) {
// 	t.plan(1);
//
// 	analyser.searchDictionary("cadeira")
// 	.then(function(res){
// 		t.same(res.class, "s.f.")
// 	});
//
// });

test('Run', run);
function run(t){
	t.plan(1);
	analyser.run("abrir a camera".split(" ")).then(function(res){
		console.log(res.command);
		t.ok();
	})
}

//test('Lexico', testLexico);
function testLexico(t) {
	//var input = ["ligue", "a", "camera", "e", "tenha", "um", "bom", "dia", "farao"];
	var input = ["os", "amigos", "de", "joao", "estranham", "seu", "modo", "de", "vestir"];

	t.plan(1);
	var start = Date.now();
	analyser.lexico(input)
	.then(function(res){
		var end = Date.now() - start;
		console.log("Time: %s miliseconds.", end);
		for(i in res) {
			console.log("%s\t\t%s", res[i].token, res[i].class);
		}
		t.same(res.length, input.length, "O numero de respostas deve ser igual ao tamanho do input.");

	});
};

//test('validacaoSintatica', validacaoSintatica)
function validacaoSintatica(t) {
	//var input = ["adj.", "art.", "s.", "v.", "art.", "s."];
	//var input = ["art.", "s.", "v.", "art.", "s."];
	t.plan(1);
	var input =
		//["v", "", "v", "art", "s"]
		// ["art.", "s.", "v.", "v.", "art.", "s."],
		 ["v", "art", "s"]
		//
	  //["s.", "v."],
		// ["s.", "s."]
	.map(function(i){ return {class:i} })
	//t.plan(1);
	analyser.validacaoSintatica(input);
	//analyser.testCommands(input)
	// .then(function(res){
		// var end = Date.now() - start;
		// console.log("Time: %s miliseconds.", end);
		// for(i in res) {
		// 	console.log("%s\t\t%s", res[i].cmd, res[i].class);
		// }
		// t.same(res.length, input.length, "O numero de respostas deve ser igual ao tamanho do input.");
		//t.same(res.length, 3, "O tamanho do array deve ser 3.");
	// });

};

//test('buscaThesauros', buscaThesauros);
function buscaThesauros(t){
	t.plan(1);

	analyser.buscaThesauros("olá")
	.then((res)=>{
		t.ok();
	})
}

//test("lexicoToValidator", lexicoToValidator);
function lexicoToValidator(t){
	var input = "Ligar a camera".split(' ');

	analyser.lexico(input)
	.then(function(res){
		console.log(res);
		return analyser.validacaoSintatica(res)
	}).then(function(res){
		console.log(res);
	})
}

// test('timing test', function (t) {
//     t.plan(2);

//     t.equal(typeof Date.now, 'function');
//     var start = Date.now();

//     setTimeout(function () {
//         t.equal(Date.now() - start, 100);
//     }, 100);
// });

// test('POST /things', function (assert) {
//   var newThing = { id: 3, name: 'New Thing'};
//   request(app)
//     .post('/things')
//     .send(newThing)
//     .expect(201)
//     .expect('Content-Type', /json/)
//     .end(function (err, res) {
//       var actualThing = res.body;

//       assert.error(err, 'No error');
//       assert.same(actualThing, newThing, 'Create a new thing');
//       assert.end();
//     });
// });

// test('GET /things', function (assert) {
//   request(app)
//     .get('/things')
//     .expect(200)
//     .expect('Content-Type', /json/)
//     .end(function (err, res) {
//       var expectedThings = [
//         { id: 1, name: 'One thing' },
//         { id: 2, name: 'Another thing' }
//       ];
//       var actualThings = res.body;

//       assert.error(err, 'No error');
//       assert.same(actualThings, expectedThings, 'Retrieve list of things');
//       assert.end();
//     });
// });
