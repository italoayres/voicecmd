var test = require('tape');
var request = require('supertest');

var app = require('./../server/app');

test('GET /cmd', function (t) {
  request(app)
    .get('/cmd/Hello World')
    //.expect(200)
    .expect('Content-Type', /json/)
    .end(function (err, res) {
      var expected = ["Hello", "World"];
      var response = res.body;
 
      t.error(err, 'No error');      
      t.same(response, expected);
      t.end();
    });
});

// test('variaçõesSemanticas', function(t) {
// 	t.plan(1);
// 	t.equal(JSON.stringify(analyser.variaçõesSemanticas("Ative o despertador")), 
// 			JSON.stringify(["Ative", "o", "despertador"]));
// });

// test('timing test', function (t) {
//     t.plan(2);

//     t.equal(typeof Date.now, 'function');
//     var start = Date.now();

//     setTimeout(function () {
//         t.equal(Date.now() - start, 100);
//     }, 100);
// });

// .post('/api/pet')
//    .send({ name: 'Manny', species: 'cat' })
//    .set('Content-Type', 'application/json')
// .get('/search')
//    .query({ query: 'Manny' })
//    .query({ range: '1..5' })
//    .query({ order: 'desc' })

// test('POST /things', function (assert) {
//   var newThing = { id: 3, name: 'New Thing'};
//   request(app)
//		
//     .post('/things')
//     .send(newThing)
//     .expect(201)
//     .expect('Content-Type', /json/)
//     .end(function (err, res) {
//       var actualThing = res.body;
 
//       assert.error(err, 'No error');
//       assert.same(actualThing, newThing, 'Create a new thing');
//       assert.end();
//     });
// });

// test('GET /things', function (assert) {
//   request(app)
//     .get('/things')
//     .expect(200)
//     .expect('Content-Type', /json/)
//     .end(function (err, res) {
//       var expectedThings = [
//         { id: 1, name: 'One thing' },
//         { id: 2, name: 'Another thing' }
//       ];
//       var actualThings = res.body;
 
//       assert.error(err, 'No error');
//       assert.same(actualThings, expectedThings, 'Retrieve list of things');
//       assert.end();
//     });
// });