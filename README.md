#Voice Command Recognition

Diretorios

- public (arquivos web estáticos)
- server
  - analyser.js (logica do analizador)
  - app.js (configuração do server e endpoints da api)
  - commands.json (arvore de commandos disponiveis)
- tests (arquivos de teste)

Para instalar as dependencias use:

    npm install

Para levantar o server:

    npm start

Rodar testes (não precisa levantar o server antes):

    npm test

Os testes do app.test.js são referentes aos endpoints da aplicação.

##Resultado

O resultado do método lexico deve ser um map com as classificações morfológicas das palavras

    ligue		 adj.2g. s.m.
    a		     s.f.2n.
    camera	 s.f.
    e		
    tenha		 adj. s.2g.
    um		   num. art. pron.
    bom		   adj. s.m.
    dia		   s.m.
    farao		 s.m.
