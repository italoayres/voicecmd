<sentença> -> <sujeito> <predicado>

<sujeito> -> <substantivo>
             | <artigo> <substantivo>
             | <artigo> <adjetivo> <substantivo>
             | <artigo> <substantivo> <adjetivo>
             | <vazio>

<predicado> -> <verbo> <objeto> | <verbo> <verbo> <objeto>

<substantivo> -> joão | Maria | cachorro | livro | pão

<artigo> -> o | a | os | as | um | uma | uns | umas

<adjetivo> -> pequeno | bom | bela

<verbo> -> corre | come | olha | é | ficou

<objeto> -> <substantivo> <objeto>
            | <artigo> <substantivo> <objeto>
            | <artigo> <adjetivo> <substantivo> <objeto>
            | <preposicao> <substantivo> <objeto>
            | <adjetivo> <substantivo> <objeto>
            | <vazio>
