var express = require('express'),
	http = require('http'),
	analyser = require('./analyser');

var app = express();

app.use('/public', express.static('public'));

app.get('/', function (req, res) {
	console.log('get /');
  	res.send('Hello World!');
});

app.get('/cmd/:cmd', function(req, res){
	if(req.params.cmd) {
		var cmds = req.params.cmd;
		// Split , Lowercase, remove special chars
		cmds = analyser.Run(cmds); // busca em APIs e analisa a palavra "Ligue a cãmera", "Ligue para João", "Entre no silencioso"
		res.json(cmds);
	} else {
		res.send('Comando não encontrado!');
	}
});

module.exports = app;
