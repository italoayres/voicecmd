var http = require('http');
var cheerio = require('cheerio');
var Q = require('q');
var rp = require('request-promise');
var commands = require('./commands.json')

var Parser = require("js-parse").Parser.LRParser;
var pd = require("./parser_description.json");

// let $ = cheerio.load('<h2 class="title">Hello world</h2>')

// $('h2.title').text('Hello there!')
// $('h2').addClass('welcome')

// $.html()

var dictionaryBaseUrl = "http://www.academia.org.br/ajax/abl/buscar-palavras?form=vocabulario&palavra=";
var thesaurosBaseUrl = "https://www.dicio.com.br/";

var Run = function(cmds) {
		var tokens = cmds.map((i)=>{ return {token:i} });
		var validationTree;

		var defer = Q.defer();

		var validacaoPromise;
		var lexPromise = lexico(cmds);
		lexPromise.then(function responseLexico(res) {
				console.log('Lexico OK', res);
				validacaoPromise = validacaoSintatica(res);
				return validacaoPromise;
		}, function err(err) { console.error(err); defer.reject("Erro na analise lexica") })
		.then(function responseValidacao(res) {
			console.log('Validação OK', res);
			validationTree = res.syn;
		}, function err(err) { console.error(err); defer.reject("Erro na validação sintática") })

		var thesPromise = buscaThesauros(cmds);
		thesPromise.then(function responseThesauros(res){
			console.log('Sinonimos OK', res);
		}, function err(err) { console.error(err); defer.reject("Erro durante a busca de sinonimos") })

		Q.spread([lexPromise, thesPromise, validacaoPromise],
		 function (lex, thes, valid){
			//var lex = res[0], thes = res[1], val = res[2];
			tokens.forEach((el, i) => {
				tokens[i].class = lex[i].class;
				tokens[i].sinonimos = thes[i];
			})

			console.log(tokens);
			return testCommands(tokens)
		})
		// }, function error(err) {
		// 	console.err(err)
		// 	defer.reject("Erro na verificação do comando");
		// })


		// busca em APIs e analisa a palavra "Ligue a cãmera", "Ligue para João", "Entre no silencioso"
		//lexico(cmds)	// ligue:verbo, a:pronome, camera:substantivo

		// Q.all([lexico(cmds).then(validacaoSintatica), buscaThesauros(cmds)])
		// .then(function success(data) {
		// 	var lex = data[0].lex, syn = data[0].syn;
		// 	var thes = data[1];
		// 	for(var i = 0; i < lex.length; i++) {
		//
		// 	}
		// 	// var lex = data[0].lex, syn = data[0].syn;
		// 	// var thes = data[1];
		// 	return testCommands(input, lex, syn, thes)
		// }, (err)=>{console.error(err); defer.reject(err)) // TODO: tratar erro

		// Busca variações da frase com a mesma semantica
			//.then(variaçõesSemanticas)	// "Abra a camera", "Ligue a máquina", "Máquina fotografica"
		// Usa o thesauros para procurar sinonimos para cada palavra
    	//.then(buscaThesauros)	// ligue: chame, telefone, chama ; a: para...
		// Testar nas variações, quais são semanticamente válidas e comparar com a lista de comandos disponíveis
      //.then(testeSinonimos)
		.then(function(match) {
				console.log('Match command found', match)
        defer.resolve({
					command: match,
					validation: validationTree
				});
    }, function err(err){
			defer.reject(err);
		});

		return defer.promise;
}

// Recebe uma lista de strings com os comandos
var lexico = function(cmds) {
    var responseList = [];
    var defer = Q.defer();
    setTimeout(function() {
        var promiseList = [];
        for (i in cmds) {
						var searchPromise = searchDictionary(cmds[i])
            promiseList[i] = searchPromise;
        }
        Q.all(promiseList).then(function(results) {
            results.forEach((result) => {
                responseList.push(result);
            });
            defer.resolve(responseList);
        });

    });
    return defer.promise;
}

var searchDictionary = function(cmd) {
    var options = {
        uri: encodeURI(dictionaryBaseUrl + cmd),
        transform: function(body) {
            return JSON.parse(body);
        }
    };
		var defered = Q.defer();

		var artigos = ["a", "o", "as", "os", "um", "uma", "uns", "umas"];
		var preposicoes = ["por", "para", "em", "de", "com"];
		if(artigos.indexOf(cmd) > -1) {
			defered.resolve({
				token: cmd,
				class: 'art'
			});
		} else if(preposicoes.indexOf(cmd) > -1) {
			defered.resolve({
				token: cmd,
				class: 'prep'
			});
		}

    var req = rp(options)
        .then(function(res) {
					//"palavra": "<span class=\"item-palavra\">palavra</span><span class=\"descricao\"> s.f.</span>"
					var classification = 0;
					if(res.rows.length > 0) {
						var $ = cheerio.load(res.rows[0].palavra);
						classification = $('.descricao').text().trim().replace(/\s+/g,"").split('.')[0];

						if(!classification) {
							var $ = cheerio.load(res.rows[1].palavra);
							classification = $('.descricao').text().trim().replace(/\s+/g,"").split('.')[0];
						}
					}
					if(classification === 0 && cmd[cmd.length-1]=='s') {
						cmd = cmd.slice(0, -1);
						searchDictionary(cmd).then(function(res){
							defered.resolve(res);
						})
					} else {
						defered.resolve({
							token: cmd,
							class: classification
						});
					}
        })
        .catch(function(err) {
            // Crawling failed or Cheerio choked...
            console.log(err);
        });
    return defered.promise;
}

var buscaThesauros = function(cmds) {
	var defered = Q.defer();
	// <p class="adicional sinonimos">
  // <a href="/adicionou/">adicionou</a>, <a href="/aditou/">aditou</a>, <a href="/adiu/">adiu</a>, <a href="/adstringiu/">adstringiu</a>, <a href="/agrupou/">agrupou</a>, <a href="/ajuntou/">ajuntou</a>, <a href="/amarrou/">amarrou</a>, <a href="/anexou/">anexou</a>, <a href="/atou/">atou</a>, <a href="/coadunou/">coadunou</a>, <a href="/colecionou/">colecionou</a>, <a href="/coligiu/">coligiu</a>, <a href="/enredou/">enredou</a>, <a href="/grupou/">grupou</a>, <a href="/ligou/">ligou</a>, <a href="/prendeu/">prendeu</a>, <a href="/reuniu/">reuniu</a>, <a href="/somou/">somou</a>, <a href="/uniu/">uniu</a></p>
	var reqList = [];
	// for each input
	cmds.forEach((cmd)=>{
		var req = rp({
				uri: encodeURI(thesaurosBaseUrl + cmd),
				transform: function(body) {

					$ = cheerio.load(body);
					var sinonimos = [];
					$('p.sinonimos a').each(function (i, el) {
						sinonimos.push($(el).html());
					})
					return sinonimos;
				}
		});
		reqList.push(req);
	})


	Q.all(reqList).then(function(responses) {
		defered.resolve(responses);
	})
	return defered.promise;
}

var validacaoSintatica = function(input) {
	var defered = Q.defer();
	var resultLex = input.map((i)=>i.class);

	// Create the parser
	var parser = Parser.CreateWithLexer(pd);

	parser.on("accept", function(token_stack){
	    console.log("Parser Accept:", require('util').inspect(token_stack, true, 1000));
			defered.resolve({
				lex: input,
				syn: token_stack
			});
	});

	parser.on("error", function(error){
	    console.log("Parse Error: ", error.message);
	    throw error.message;
			defered.reject(error);
	});

	// Begin processing the input
	for(var i in resultLex) {
	    parser.append(input[i].class);
			parser.append(" ");
	}
	parser.end();

	return defered.promise;
}

function testCommands(tokens) {
	var defer = Q.defer();

	tokens.forEach((item, i)=>{
		item.sinonimos.forEach(function(sinonimo){
			for(var i in commands) {
				if(commands[i].indexOf(sinonimo) > -1 || commands[i].indexOf(item.token) > -1)
				{
					// Comando encontrado
					var cmd = i;
					var parameters = [];
					tokens.forEach((item, i)=>{
						if(item.class == 's') parameters.push(item.token);
					})
					if(parameters.length == 0) defer.reject("Não há informações suficientes para completar a operação");
					defer.resolve({ action: cmd, parameters: parameters });
					break;
				}
			}

		})
		defer.reject("Comando não encontrado")
	})
	return defer.promise;
}

exports.run = Run;
exports.lexico = lexico;
exports.searchDictionary = searchDictionary;
exports.buscaThesauros = buscaThesauros;
exports.testCommands = testCommands;
exports.validacaoSintatica = validacaoSintatica;
