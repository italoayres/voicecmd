var flag = true; // Variável de controle para obter status de exibição

/* function exibe menu */
function exibeMenu(divid){
	document.getElementById(divid).style.display = "block";
}

/* function esconde menu */
function escondeMenu(divid){
	document.getElementById(divid).style.display = "none";
}

/* function de controle de exibição do conteúdo mobile */
function exibeMobile(){
	document.getElementById("voice").style.display = "block";	
	document.getElementById("close").style.display = "block";
	document.getElementById("saida-voice").style.display = "block";

	if (flag == true) {
		/* Esconde o icone microfone */
		document.getElementById("microfone").style.display = "none";
		flag = false;
	} else {
		/* Esconde o voice animation e o close, e exibe o icone microfone */
		document.getElementById("voice").style.display = "none";	
		document.getElementById("close").style.display = "none";
		document.getElementById("saida-voice").style.display = "none";
		document.getElementById("iphone").src = "img/iphone.png";
		document.getElementById("microfone").style.display = "block";
		flag = true;		
	}
}