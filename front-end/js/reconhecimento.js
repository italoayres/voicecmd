var recognizing = false;
var final_span = document.getElementById('final_span');
var final_span_acao = document.getElementById('final_span_acao');

if ('webkitSpeechRecognition' in window) {

	var recognition = new webkitSpeechRecognition();
	recognition.continuous = true;
	recognition.interimResults = true;

	recognition.onstart = function() {
		recognizing = true;
	};

	recognition.onend = function() {
    	recognizing = false;
    }	

	recognition.onresult = function(event) {
		var interim_transcript = '';

		for (var i = event.resultIndex; i < event.results.length; ++i) {
			interim_transcript += event.results[i][0].transcript;
			controle = true;
		}
		final_span.innerHTML = interim_transcript;

		/*
		var verbos = ["abrir", "abra", "fechar", "feche", "tirar", "tire", "ligar", "telefonar", "escrever"];
		var artigos = ["um", "uma", "uns", "a", "o"];
		var substantivos = ["câmera", "foto", "galeria", "imagens", "tempo", "clima", "hora", "relógio"];
		*/
		var comandos = ["Abrir a câmera", "tirar foto", "tirar Fotografia", "Ligar para João", "falar com João", "Telefonar para João", "mensagem para Pedro", "escrever para Pedro", "Abrir mensagem"];

		var final_gramatical = document.getElementById("area_gramatical");

		var breaks = interim_transcript.split(" ");
		var newLines = "";
		for (var i = 0; i < breaks.length; i++) {
			// ENVIAR_PARA_SERVIDOR = breaks[i];
			newLines = newLines + breaks[i] + " - CLASSIFICAÇÃO " + "\n" ;
			final_gramatical.innerHTML = newLines;			
		}

		if ((interim_transcript == (comandos[0])) || (interim_transcript == (comandos[1])) || (interim_transcript == (comandos[2]))) {
			final_span_acao.innerHTML = "ABRINDO A CAMERA...";
			setTimeout(function(){
				document.getElementById("iphone").src = "img/camera.png";
				document.getElementById("voice").style.display = "none";
				document.getElementById("saida-voice").style.display = "none";
				document.getElementById("close").style.display = "none";	
				document.getElementById("microfone").style.display = "none";
			}, 1500);
		} 

		if ((interim_transcript == (comandos[3])) || (interim_transcript == (comandos[4])) || (interim_transcript == (comandos[5]))) {
			final_span_acao.innerHTML = "LIGANDO PARA JOÃO...";
			setTimeout(function(){
				document.getElementById("iphone").src = "img/ligacao.png";
				document.getElementById("voice").style.display = "none";
				document.getElementById("saida-voice").style.display = "none";
				document.getElementById("close").style.display = "none";	
				document.getElementById("microfone").style.display = "none";
			}, 1500);
		} 

		if ((interim_transcript == (comandos[6])) || (interim_transcript == (comandos[7])) || (interim_transcript == (comandos[8]))) {
			final_span_acao.innerHTML = "ABRINDO CAIXA DE MENSAGEM...";
			setTimeout(function(){
				document.getElementById("iphone").src = "img/mensagem.png";
				document.getElementById("voice").style.display = "none";
				document.getElementById("saida-voice").style.display = "none";
				document.getElementById("close").style.display = "none";	
				document.getElementById("microfone").style.display = "none";
			}, 1500);
		}

		if ((interim_transcript != (comandos[0])) && (interim_transcript != (comandos[1])) && (interim_transcript != (comandos[2])) && (interim_transcript && (comandos[3])) && (interim_transcript != (comandos[4])) && (interim_transcript != (comandos[5])) && (interim_transcript != (comandos[6])) && (interim_transcript != (comandos[7])) && (interim_transcript != (comandos[8]))) {
				final_span_acao.innerHTML = "COMANDO NÃO É VÁLIDO...";
		}

	};
}

function startButton(event) {
	if (recognizing) {
		return;
	}
	recognition.lang = 'pt-BR';
	recognition.start();
	ignore_onend = false;
	final_span.innerHTML = '';

}

// http.get("/comando", {final_transcript}, executar)

/**
cmd:
{ action: "abrir", parameter: "camera" }
{ action: "chamar", parameter: "joao" }  
{ action: "mensagem", parameter: "pedro" }   

function executar(cmd) {
  switch (cmd.action) {
    case 'executar'
      openApp(cmd.parameter)
    case 'msg'
      sendMessage(cmd.parameter)
    case 'call'
      startCall(cmd.parameter)
  }
}

openApp(name) {
	execute({ action: "abrir", parameter: "camera" }); 
}

sendMessage(contactName) {
	execute({ action: "mensagem", parameter: "joao" }); 
}

startCall(contactName) {
	execute({ action: "chamar", parameter: "pedro" }); 
}
**/